import React from 'react'
import {Switch, Route, Link} from 'react-router-dom'
import './App.css'
import Title from './components/Title'
import Image from './components/Image'
import Price from './components/Price'
import Date from './components/Date'

const ToursAPI = {
  tours: [
    {id: 1, name: "England", price: "200$", image: "/dublin.jpg", availableFrom: "2019-10-19", location:{city:"London", street:"Backer", house:"13"}},
    {id: 2, name: "Italy", price: "300$", image: "/dublin.jpg", availableFrom: "2019-11-11", location:{city:"Rome", street:"Backer1", house:"12"}},
    {id: 3, name: "Japan", price: "250$", image: "/dublin.jpg", availableFrom: "2019-12-09", location:{city:"Tokio", street:"Backer2", house:"11"}},
    {id: 4, name: "Ukraine", price: "1000$", image: "/dublin.jpg", availableFrom: "2019-11-03", location:{city:"Lviv", street:"Backer3", house:"10"}},
],
  all: function() {return this.tours},
  get: function(id) {
      const isTour = tour => tour.id === id
      return this.tours.find(isTour)
  }
}

const ToursList = () => (
  <div className="container">
    <h1>Tours</h1>
    <div className="itemList">
      {
        ToursAPI.all().map(tours => (
          <div className="item" key = {tours.id}>
              <Link to={`/tours/${tours.id}`}>
                  <div className='itemInfo'>
                      <div>From: <Date>{tours.availableFrom}</Date></div>
                      <Price>{tours.price}</Price>
                  </div>
                  <Image>{tours.image}</Image>
                  <Title>{tours.name}</Title>
              </Link>
          </div>
        ))
      }
    </div>
  </div>
)
const Tour = (props) => {
  const tour = ToursAPI.get(
    parseInt(props.match.params.id, 10)
  )
  if (!tour) {
    return <div>Sorry, tour is over</div>
  }
  return (
    <div className="tour">
      <div className="container">
        <Link to="/tours" className="tourBack">Back</Link>
        <Title>{tour.name}</Title>
        <div className="tourInformation">
        <div className="tourLeft side">
          <Image>{tour.image}</Image>
        </div>
        <div className="tourRight side">
          <p><span>Location:</span> {tour.location.city}</p>
          <p><span>Street:</span> {tour.location.street}, {tour.location.house}</p>
          <p><span>Available from:</span> {tour.availableFrom}</p>
          <p><span>Price:</span> <span className="price">{tour.price}</span></p>
          <button className="book">Book</button>
        </div>
        </div>
      </div>
    </div>
  )
}

const Tours = () => (
  <div>
    <Switch>
      <Route exact path='/tours' component={ToursList}/>
      <Route path='/tours/:id' component={Tour}/>
    </Switch>
  </div>
)
// {/*  */}
const Home = () => (
  <div>
    <h1>Home page</h1>
  </div>
)

const Main = () => (
  <main>
    <Switch>
      <Route exact path='/' component={Home}/>
      <Route path='/tours' component={Tours}/>
    </Switch>
  </main>
)

const Header = () => (
  <header>
    <div className="container">
      <ul className="nav">
        <li><Link to='/'>Home</Link></li>
        <li><Link to='/tours'>Tours</Link></li>
      </ul>
    </div>
  </header>
)
const App = () => (
  <div>
    <Header />
    <Main />
  </div>
)

export default App;

