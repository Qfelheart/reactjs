import React from 'react'

const Price = ({children}) => <span className='price'>{children} per/day</span>
export default Price