import React from 'react'
import Moment from 'react-moment'

const Date = ({children}) => <Moment format="DD/MM/YYYY" className='date'>{children}</Moment>
export default Date