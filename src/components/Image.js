import React from 'react'

const Image = ({children}) => <img src={children} alt='img' />
export default Image